﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using GrowinHouseManagement.Interfaces;
using GrowinHouseManagement.Mediator;
using GrowinHouseManagement.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace GrowinHouseManagement.Controllers
{
    [Route("api/Pessoa")]
    [ApiController]
    public class PessoaController : Controller/*: BaseController<Pessoa>*/
    {
        private IMediator _mediator;
        private IMapper _mapper;
        public PessoaController([FromServices] IMediator mediator, [FromServices] IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }

        [HttpGet]
        [Route("{id:long}")]
        public async Task<ActionResult<PessoaViewModel>> Get(long id)
        {
            try
            {
                var pessoa = await _mediator.Send(new PessoaGetRequest
                {
                    Id = id
                });
                return _mapper.Map<PessoaViewModel>(pessoa);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }


        [HttpGet]
        public async Task<ActionResult<List<PessoaViewModel>>> GetAll()
        {
            try
            {
                var pessoas = await _mediator.Send(new PessoaGetAllRequest
                {

                });
                return _mapper.Map<List<PessoaViewModel>>(pessoas);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpPost]
        public async Task<ActionResult<Pessoa>> Post(Pessoa pessoa)
        {
            try
            {
                return await _mediator.Send(new PessoaPostRequest
                {
                    Pessoa = pessoa
                });
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
    }
}