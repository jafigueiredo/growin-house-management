﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using GrowinHouseManagement.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using MediatR;
using GrowinHouseManagement.Mediator;
using GrowinHouseManagement.Models;

namespace GrowinHouseManagement.Controllers
{
    public class BaseController<Entity> : Controller where Entity : class
    {
        //public IMediator _mediator;

        //public BaseController([FromServices] IMediator mediator)
        //{
        //    _mediator = mediator;
        //}

        //[HttpGet]
        //[Route("{id:long}")]
        //public async Task<ActionResult<Entity>> Get(long id)
        //{
        //    try
        //    {
        //        return await _mediator.Send(new GenericGetByIdRequest<Entity>
        //        {
        //            Id = id
        //        });
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest();
        //    }
        //}

        //[HttpPost]
        //public async Task<ActionResult<Entity>> Post(Entity entity)
        //{
        //    try
        //    {
        //        return await _mediator.Send(new GenericPostRequest<Entity>
        //        {
        //            entity = entity
        //        });
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest();
        //    }
        //}
    }
}