﻿using GrowinHouseManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrowinHouseManagement.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        //repositories
        IRepository<Pessoa> PessoaRepository { get; }

        IRepository<T> GetRepository<T>() where T : class;

        bool IsActive { get; }
        bool WasCommitted { get; }
        //void Update<T>(Dictionary<string, string> props);
        void Commit();
        void Rollback();
    }
}
