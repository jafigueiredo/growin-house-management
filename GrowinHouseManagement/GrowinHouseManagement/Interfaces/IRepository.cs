﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrowinHouseManagement.Interfaces
{
    public interface IRepository<T> where T : class
    {
        Task<T> GetByIdAsync(long id);

        T GetBy(string attrName, object attrValue);

        Task<List<T>> GetAllAsync();

        IQueryable<T> GetAllQueryable();

        void Add(T entity);

        Task<T> AddAsync(T entity);

        void AddMany(IEnumerable<T> entities);

        void Delete(T entity);

        void DeleteMany(IEnumerable<T> entities);

        void Update(T entity);
    }
}
