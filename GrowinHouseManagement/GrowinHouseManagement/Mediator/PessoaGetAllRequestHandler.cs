﻿using GrowinHouseManagement.Interfaces;
using GrowinHouseManagement.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GrowinHouseManagement.Mediator
{
    public class PessoaGetAllRequestHandler : IRequestHandler<PessoaGetAllRequest, List<Pessoa>>
    {
        private readonly IUnitOfWork _uow;

        public PessoaGetAllRequestHandler([FromServices] IUnitOfWork uow)
        {
            _uow = uow;
        }

        public async Task<List<Pessoa>> Handle(PessoaGetAllRequest request, CancellationToken cancellationToken)
        {
            return await _uow.PessoaRepository.GetAllAsync();
        }
    }
}
