﻿using GrowinHouseManagement.Interfaces;
using GrowinHouseManagement.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GrowinHouseManagement.Mediator
{
    public class PessoaGetRequestHandler : IRequestHandler<PessoaGetRequest, Pessoa>
    {
        private readonly IUnitOfWork _uow;

        public PessoaGetRequestHandler([FromServices] IUnitOfWork uow)
        {
            _uow = uow;
        }

        public async Task<Pessoa> Handle(PessoaGetRequest request, CancellationToken cancellationToken)
        {
            return await _uow.PessoaRepository.GetByIdAsync(request.Id);
        }
    }
}