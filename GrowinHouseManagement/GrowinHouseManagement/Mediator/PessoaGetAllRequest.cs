﻿using GrowinHouseManagement.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrowinHouseManagement.Mediator
{
    public class PessoaGetAllRequest : IRequest<List<Pessoa>>
    {
    }
}
