﻿using GrowinHouseManagement.Models;
using MediatR;

namespace GrowinHouseManagement.Mediator
{
    public class PessoaPostRequest : IRequest<Pessoa>
    {
        public Pessoa Pessoa { get; set; }
    }
}
