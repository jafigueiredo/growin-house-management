﻿using GrowinHouseManagement.Interfaces;
using GrowinHouseManagement.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GrowinHouseManagement.Mediator
{
    public class PessoaPostRequestHandler : IRequestHandler<PessoaPostRequest, Pessoa>
    {
        private readonly IUnitOfWork _uow;

        public PessoaPostRequestHandler([FromServices] IUnitOfWork uow)
        {
            _uow = uow;
        }

        public async Task<Pessoa> Handle(PessoaPostRequest request, CancellationToken cancellationToken)
        {
            var ret = await _uow.PessoaRepository.AddAsync(request.Pessoa);
            _uow.Commit();
            return request.Pessoa;
        }
    }
}
