﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrowinHouseManagement.Models
{
    public class VisitaViewModel
    {
        public long Id { get; set; }
        public DateTime HoraEntrada { get; set; }
        public DateTime HoraSaida { get; set; }
        public string MotivoVisita { get; set; }
        public string Piso { get; set; }
        public long PessoaId { get; set; }
        public virtual PessoaViewModel Pessoa { get; set; }
    }
}
