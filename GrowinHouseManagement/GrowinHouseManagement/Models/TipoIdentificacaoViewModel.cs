﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrowinHouseManagement.Models
{
    public class TipoIdentificacaoViewModel
    {
        public long Id { get; set; }

        public string NomeIdentificacao { get; set; }

    }
}
