﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrowinHouseManagement.Models
{
    public class PessoaViewModel
    {
        public long Id { get; set; }
        public string PrimeiroNome { get; set; }
        public string UltimoNome { get; set; }
        public string NumeroIdenficacao { get; set; }

        public long TipoIdentificacaoId { get; set; }
        public virtual TipoIdentificacaoViewModel TipoIdentificacao { get; set; }

        public virtual ICollection<VisitaViewModel> Visita { get; set; }
    }
}
