﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrowinHouseManagement.Models.Mapper
{
    public static class GrowinHouseMapper
    {
        public static MapperConfiguration InitializeAutoMapper()
        {
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<DomainToViewModelMappingProfile>();
                //cfg.ForAllMaps((typeMap, mappingExpression) => mappingExpression.MaxDepth(2));
            });
            return mapper;
        }
    }
}
