﻿using AutoMapper;
using GrowinHouseManagement.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrowinHouseManagement.Models.Mapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<Pessoa, PessoaViewModel>().ReverseMap();
            CreateMap<Visita, VisitaViewModel>().ReverseMap();
            CreateMap<TipoIdentificacao, TipoIdentificacaoViewModel>().ReverseMap();
        }
    }
}
