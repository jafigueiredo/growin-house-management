﻿using GrowinHouseManagement.Domain;
using GrowinHouseManagement.Interfaces;
using GrowinHouseManagement.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;

namespace GrowinHouseManagement
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool _disposed;
        //repositories
        private IRepository<Pessoa> _PessoaRepository { get; set; }
        private IRepository<Auditory> _AuditoryRepository { get; set; }

        public GrowinHouseContext _context { get; set; }

        public UnitOfWork(GrowinHouseContext siipcontext)
        {
            _context = siipcontext;
        }

        public void RegisterNew<T>(T entity) where T : class
        {
            _context.Entry(entity).State = EntityState.Added;
        }

        //repositoriesproperties
        public IRepository<Pessoa> PessoaRepository { get { if (_PessoaRepository == null) _PessoaRepository = new Repository<Pessoa>(_context); return _PessoaRepository; } }
        public IRepository<Auditory> AuditoryRepository { get { if (_AuditoryRepository == null) _AuditoryRepository = new Repository<Auditory>(_context); return _AuditoryRepository; } }

        public void Commit()
        {
            var modifiedEntries = _context.ChangeTracker.Entries().Where(x => (x.State == EntityState.Added || x.State == EntityState.Modified || x.State == EntityState.Deleted) && x.Entity.GetType().Name != "Log");
            _context.SaveChanges();
            //foreach (var entry in modifiedEntries)
            //{
            //    Auditory audit = new Auditory();
            //    object entity = entry.Entity;
            //    if (entity != null)
            //    {
            //        string identityName = WindowsIdentity.GetCurrent().Name;
            //        DateTime now = DateTime.Now;

            //        if (entry.State == EntityState.Added)
            //        {
            //            audit.CriadoPor = identityName;
            //            audit.CriadoEm = now;
            //        }
            //        else
            //        {
            //            audit.EntityId = (entity.GetType().GetProperty("Id") == null) ?
            //                0 : (long)entity.GetType().GetProperty("Id").GetValue(entity);
            //        }
            //        audit.AlteradoPor = identityName;
            //        audit.AlteradoEm = now;
            //        audit.TableName = (string)entity.GetType().Name;
            //    }

            //    AuditoryRepository.Add(audit);
            //}
            //_context.SaveChanges();
        }

        public IRepository<T> GetRepository<T>() where T : class
        {
            return new Repository<T>(_context);
        }
        public void Rollback() => throw new NotImplementedException();

        public bool IsActive => throw new NotImplementedException();

        public bool WasCommitted => throw new NotImplementedException();

        #region IDisposable Support

        private bool disposedValue; // To detect redundant calls

        public void Dispose()
        {
            if (_disposed)
                return;

            _disposed = true;
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        #endregion
    }
}
