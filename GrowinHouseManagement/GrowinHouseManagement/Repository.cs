﻿using GrowinHouseManagement.Domain;
using GrowinHouseManagement.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrowinHouseManagement
{
    public class Repository<Entity> : IRepository<Entity> where Entity : class
    {
        private readonly GrowinHouseContext Context;
        private DbSet<Entity> _entities;

        public Repository(GrowinHouseContext context)
        {
            this.Context = context;
            _entities = context.Set<Entity>();
        }

        public async Task<Entity> GetByIdAsync(long id)
        {
            return await _entities.FindAsync(id);
        }

        public Entity GetBy(string attrName, object attrValue)
        {
            if (typeof(Entity).GetProperty(attrName) == null) return null;
            var entities = GetAllQueryable();
            return entities.Where(x => x.GetType().GetProperty(attrName).GetValue(x) == attrValue).FirstOrDefault();
        }

        public async Task<List<Entity>> GetAllAsync()
        {
            return await _entities.ToListAsync();
        }

        public IQueryable<Entity> GetAllQueryable()
        {
            return _entities.AsQueryable();
        }

        public void Add(Entity entity)
        {
            _entities.Add(entity);
        }

        public async Task<Entity> AddAsync(Entity entity)
        {
            await _entities.AddAsync(entity);
            return entity;
        }

        public void AddMany(IEnumerable<Entity> entities)
        {
            _entities.AddRange(entities);
        }

        public void Delete(Entity entity)
        {
            _entities.Remove(entity);
        }

        public void DeleteMany(IEnumerable<Entity> entities)
        {
            _entities.RemoveRange(entities);
        }

        public virtual void Update(Entity entityToUpdate)
        {
            _entities.Attach(entityToUpdate);
            foreach (var prop in entityToUpdate.GetType().GetProperties())
            {
                if (prop.GetValue(entityToUpdate).GetType().Namespace == "GrowinHouseManagement.Models")
                {
                    (Context.GetType().GetProperty(prop.Name).GetValue(Context)).GetType().GetMethod("Attach").Invoke(Context.GetType().GetProperty(prop.Name).GetValue(Context), new[] { prop.GetValue(entityToUpdate) });
                    Context.Entry(prop.GetValue(entityToUpdate)).State = EntityState.Modified;
                }
            }
            Context.Entry(entityToUpdate).State = EntityState.Modified;
        }
    }
}
