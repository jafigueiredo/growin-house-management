﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GrowinHouseManagement.Domain.Migrations
{
    public partial class second : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TipoIdentificacao",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NomeIdentificacao = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoIdentificacao", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Visita",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    HoraEntrada = table.Column<DateTime>(nullable: false),
                    HoraSaida = table.Column<DateTime>(nullable: false),
                    MotivoVisita = table.Column<string>(nullable: true),
                    Piso = table.Column<string>(nullable: true),
                    PessoaId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Visita", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Visita_Pessoa_PessoaId",
                        column: x => x.PessoaId,
                        principalTable: "Pessoa",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Pessoa_TipoIdentificacaoId",
                table: "Pessoa",
                column: "TipoIdentificacaoId");

            migrationBuilder.CreateIndex(
                name: "IX_Visita_PessoaId",
                table: "Visita",
                column: "PessoaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Pessoa_TipoIdentificacao_TipoIdentificacaoId",
                table: "Pessoa",
                column: "TipoIdentificacaoId",
                principalTable: "TipoIdentificacao",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Pessoa_TipoIdentificacao_TipoIdentificacaoId",
                table: "Pessoa");

            migrationBuilder.DropTable(
                name: "TipoIdentificacao");

            migrationBuilder.DropTable(
                name: "Visita");

            migrationBuilder.DropIndex(
                name: "IX_Pessoa_TipoIdentificacaoId",
                table: "Pessoa");
        }
    }
}
