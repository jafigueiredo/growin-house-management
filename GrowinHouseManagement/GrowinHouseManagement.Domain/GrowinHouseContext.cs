﻿using GrowinHouseManagement.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrowinHouseManagement.Domain
{
    public class GrowinHouseContext : DbContext
    {
        public GrowinHouseContext(DbContextOptions<GrowinHouseContext> options) : base(options)
        {

        }

        public DbSet<Pessoa> Pessoa { get; set; }
        public DbSet<TipoIdentificacao> TipoIdentificacao { get; set; }
        public DbSet<Auditory> Auditory { get; set; }
        public DbSet<Visita> Visita { get; set; }
        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Pessoa>()
        //        .HasMany(p => p.Visita)
        //        .WithOne(v => v.Pessoa)
        //        .HasForeignKey(p => p.PessoaId);

        //    modelBuilder.Entity<TipoIdentificacao>()
        //        .HasMany(t => t.Pessoa)
        //        .WithOne(p => p.TipoIdentificacao)
        //        .HasForeignKey(p => p.TipoIdentificacaoId);
        //}
    }
}
