﻿using GrowinHouseManagement.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GrowinHouseManagement.Models
{
    public class Pessoa
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string PrimeiroNome { get; set; }
        public string UltimoNome { get; set; }
        public string NumeroIdenficacao { get; set; }

        [ForeignKey("TipoIdentificacao")]
        public long TipoIdentificacaoId { get; set; }
        public virtual TipoIdentificacao TipoIdentificacao { get; set; }

        public virtual ICollection<Visita> Visita { get; set; }
    }
}
