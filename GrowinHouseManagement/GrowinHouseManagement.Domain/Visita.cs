﻿using GrowinHouseManagement.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace GrowinHouseManagement.Domain
{
    public class Visita
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public DateTime HoraEntrada { get; set; }
        public DateTime HoraSaida { get; set; }
        public string MotivoVisita { get; set; }
        public string Piso { get; set; }

        [ForeignKey("Pessoa")]
        public long PessoaId { get; set; }
        public virtual Pessoa Pessoa { get; set; }
    }
}
