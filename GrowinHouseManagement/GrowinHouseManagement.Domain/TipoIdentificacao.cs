﻿using GrowinHouseManagement.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace GrowinHouseManagement.Domain
{
    public class TipoIdentificacao
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public string NomeIdentificacao { get; set; }

        public virtual ICollection<Pessoa> Pessoa { get; set; }

    }
}
