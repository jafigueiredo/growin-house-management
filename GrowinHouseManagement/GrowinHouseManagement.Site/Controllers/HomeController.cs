﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using GrowinHouseManagement.Site.Models;
using GrowinHouseManagement.Site.Mediator;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using GrowinHouseManagement.Models;

namespace GrowinHouseManagement.Site.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        public IMediator _mediator;

        public HomeController(ILogger<HomeController> logger, [FromServices] IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [Authorize]
        public async Task<IActionResult> Index()
        {
            try
            {
                var pessoas = await _mediator.Send(new HttpPessoaGetAllRequest
                {

                });

                return View(pessoas);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> CreatePessoa(PessoaViewModel pessoa)
        {
            try
            {
                if (!ModelState.IsValid)
                { // re-render the view when validation failed.
                    return View("Home");
                }

                var pessoas = await _mediator.Send(new HttpPessoaPostRequest
                {
                    Pessoa = pessoa
                });

                TempData["notice"] = "Person successfully created";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [Authorize]
        [HttpGet]
        public IActionResult CreatePessoa()
        {
            return View("Pessoa");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
