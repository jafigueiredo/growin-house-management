﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrowinHouseManagement.Site.HTTP
{
    public interface IClientHttp
    {
        string ApiAdress { get; set; }
        Task<T> asyncGetCall<T>(string Url);

        Task<T> asyncPostCall<T>(string Url, object Content);
    }
}
