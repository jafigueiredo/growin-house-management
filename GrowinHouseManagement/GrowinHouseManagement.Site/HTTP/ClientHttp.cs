﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace GrowinHouseManagement.Site.HTTP
{
    public class ClientHttp : IClientHttp
    {
        public string ApiAdress { get; set; }
        public ClientHttp(string apiAddress)
        {
            ApiAdress = apiAddress;
        }

        public async Task<T> asyncGetCall<T>(string Url)
        {
            object toReturn = Activator.CreateInstance(typeof(T));

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(ApiAdress);
                    HttpResponseMessage response = client.GetAsync(Url).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var jsonString = await response.Content.ReadAsStringAsync();

                        toReturn = JsonConvert.DeserializeObject<T>(jsonString);
                    }
                    else
                    {
                        toReturn = "Erro Interno de Servidor";                           
                    }
                }
            }
            catch (Exception ex)
            {
                    
            }

            return (T)toReturn;
        }

        public async Task<T> asyncPostCall<T>(string Url, object Content)
        {
            object toReturn = Activator.CreateInstance(typeof(T));

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(ApiAdress);
                    HttpResponseMessage response = await client.PostAsync(Url, new StringContent(JsonConvert.SerializeObject(Content), System.Text.Encoding.UTF8, "application/json"));

                    if (response.IsSuccessStatusCode)
                    {
                        var jsonString = await response.Content.ReadAsStringAsync();
                        toReturn = JsonConvert.DeserializeObject<T>(jsonString);
                        if (string.IsNullOrEmpty((string)toReturn)) toReturn = new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
                    }
                    else
                    {
                        toReturn = "Erro Interno de Servidor";
                    }
                }
            }
            catch (Exception ex)
            {
                    
            }
            return (T)toReturn;
        }

    }
}
