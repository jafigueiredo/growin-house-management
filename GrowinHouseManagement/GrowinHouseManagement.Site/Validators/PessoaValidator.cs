﻿using FluentValidation;
using GrowinHouseManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrowinHouseManagement.Site.Validators
{
    public class PessoaValidator : AbstractValidator<PessoaViewModel>   
    {
		public PessoaValidator()
		{
			RuleFor(x => x.PrimeiroNome).Length(0, 10).NotNull().NotEmpty();
			RuleFor(x => x.UltimoNome).Length(0, 15).NotNull().NotEmpty();
			RuleFor(x => x.NumeroIdenficacao).Length(0, 15).NotNull().NotEmpty();
			RuleFor(x => x.TipoIdentificacaoId).GreaterThan(0).NotNull().NotEmpty();
		}
	}
}
