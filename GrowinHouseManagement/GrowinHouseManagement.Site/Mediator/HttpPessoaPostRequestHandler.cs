﻿using GrowinHouseManagement.Models;
using GrowinHouseManagement.Site.HTTP;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GrowinHouseManagement.Site.Mediator
{
    public class HttpPessoaPostRequestHandler : IRequestHandler<HttpPessoaPostRequest, PessoaViewModel>
    {
        IClientHttp _clientHttp;
        public HttpPessoaPostRequestHandler([FromServices] IClientHttp clientHttp)
        {
            _clientHttp = clientHttp;
        }

        public async Task<PessoaViewModel> Handle(HttpPessoaPostRequest request, CancellationToken cancellationToken)
        {
            return await _clientHttp.asyncPostCall<PessoaViewModel>($"Pessoa", request.Pessoa);
        }
    }
}
