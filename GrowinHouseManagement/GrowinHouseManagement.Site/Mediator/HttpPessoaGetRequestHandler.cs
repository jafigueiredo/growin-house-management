﻿using GrowinHouseManagement.Models;
using GrowinHouseManagement.Site.HTTP;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GrowinHouseManagement.Site.Mediator
{
    public class HttpPessoaGetRequestHandler : IRequestHandler<HttpPessoaGetRequest, PessoaViewModel>
    {
        IClientHttp _clientHttp;
        public HttpPessoaGetRequestHandler([FromServices] IClientHttp clientHttp)
        {
            _clientHttp = clientHttp;
        }

        public async Task<PessoaViewModel> Handle(HttpPessoaGetRequest request, CancellationToken cancellationToken)
        {
            return await _clientHttp.asyncGetCall<PessoaViewModel>($"Pessoa/{request.Id}");
        }
    }
}
