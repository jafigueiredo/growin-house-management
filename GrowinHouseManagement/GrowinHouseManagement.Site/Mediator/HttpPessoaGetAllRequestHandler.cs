﻿using GrowinHouseManagement.Models;
using GrowinHouseManagement.Site.HTTP;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GrowinHouseManagement.Site.Mediator
{
    public class HttpPessoaGetAllRequestHandler : IRequestHandler<HttpPessoaGetAllRequest, List<PessoaViewModel>>
    {
        IClientHttp _clientHttp;
        public HttpPessoaGetAllRequestHandler([FromServices] IClientHttp clientHttp)
        {
            _clientHttp = clientHttp;
        }

        public async Task<List<PessoaViewModel>> Handle(HttpPessoaGetAllRequest request, CancellationToken cancellationToken)
        {
            return await _clientHttp.asyncGetCall<List<PessoaViewModel>>("Pessoa");
        }
    }
}
