﻿using GrowinHouseManagement.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrowinHouseManagement.Site.Mediator
{
    public class HttpPessoaGetRequest : IRequest<PessoaViewModel>
    {
        public long Id { get; set; }
    }
}
